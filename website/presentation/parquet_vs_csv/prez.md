# CSV ou Parquet ?

---

## Pourquoi ?
<!-- .slide: class="slide" -->


Un projet **la dataplatform** qui regroupe plusieurs projets:

- Melodi
- Aida
- ArchimEd 

Comment stocker les données nécéssaire à ces 3 applications ?

---

<!-- .slide: class="slide" -->

<img src="./img/si-ddar.drawio.png" alt="schema dataplatform" width="45%"> 

---

## Quelle utilité pour les données stockées ?
<!-- .slide: class="slide" -->


Des données prêtes pour de la diffusion, possiblement des jeux de données avec "beaucoup" de colonnes. Le but de ces données étant de:
- Aida:
  * Réaliser de cubes (réalisation de tableaux croisé)
  * Réaliser des études de territoires sur des variables précises (aggrégation)
- Melodi:
  * Mettre à disposition des données aggrègé par le biais d'une API.
- ArchimEd:
  * Gérer leur cycle de vie: de la création à l archivage par les archives nationale.
  * Mise à disposition des données

-> Un besoin de type analytique

---

## Quel format de donnée recherché ?
<!-- .slide: class="slide" -->


Un format de donnée efficace pour :

- Projection: le fait de selectionner des colonnes, c'est le fait de selectionner uniquement certaines colonnes (le **SELECT** en SQL)
- Prédicats push down: le critère pour selectionner des lignes, c'est la possibilité de filtrer des données selon une ou plusieurs valeurs (le **WHERE** dans sql)

---

## Quel format pour les données stockées ?
<!-- .slide: class="slide" -->


2 format de données à comparer:

- CSV
- parquet

2 autres contraintes à respecter:

- Vitesse de lecture (user experience)
- Taille sur disque (contrainte de production)

---

## Le format CSV
<!-- .slide: class="slide" -->


- format de données textuel orientés ligne
- chaque champs est séparé par une virgule (ou autre)
- fhumainement lisible
- format interopérable (c'est du texte)


```csv
Name,email,phone number,address
Example,example@example.com,555-555-5555,Example Address
```

---

## Le format parquet
<!-- .slide: class="slide" -->

- format de données orienté colonne (voir même hybride -> on verra après)
- contient des métadonnées
- format interopérable (R, python, spark, java, et même SAS viya)
- C'est un binaire

---

![](./img/parquet-schema.png)

---

## Comment trancher ? (1/4)
<!-- .slide: class="slide" -->


- ✔️ Parquet: les fichiers sont compressés colonne par colonne (en fonction de leur type de données: entier, chaîne de caractères, date). Réduction de x10 de la taille par rapport a du csv. La différence est plus faible si on travaille avec du csv compressé.
- ✔️ Parquet: determiner format d'une colonne
- ✔️ CSV: Nativement humainement lisible 

---

## Comment trancher ? (2/4)
<!-- .slide: class="slide" -->


![](./img/compare_format.webp)

ℹ️ la projection est plus facile avec le format hybride ou l'orienté colonne

---

## Comment trancher ? (3/4)
<!-- .slide: class="slide" -->


![](./img/parquet_details.webp)

ℹ️ le predicats est plus facile avec le format hybride

⚠️ La jointure horizontale n'est pas facile

---

## Comment trancher ? (4/4)
<!-- .slide: class="slide" -->

- Optimisation proposé par parquet:
  * Run-Length Encoding (RLE): Si répétition d'une même valeurs, une seule valeur est stockée.
  * Dictionnary encoding: association des longues chaines de caractères a une liste de clé valeurs stockées dans les métadonnées

---

## Quels avantages pour le dev ?
<!-- .slide: class="slide" -->


- Métadonnées dans le fichier
- Plus léger que le csv (facteur 10)
- Plus rapide en lecture:
  * si je n'ai besoin que de certaines colonnes (mais pas forcément si chargement complet de la table)
  * Requiert un bon partitionnement des données.
- Plus léger a charger en mémoire (on ne charge que ce dont on a besoin)
- (Natif avec le monde spark/hadoop)
- Peut etre utilisé par des outils tels que Trino, duckDb, etc...

---

## Autres cas d'usage
<!-- .slide: class="slide" -->


- alternative a une bdd peut evolutive

⚠️ Attention plus de log/plus de backup. Par contre, la logique de traitements de données se situe dans le code et non au niveau de la bdd

---

## Choix dans la dataplatform

<!-- .slide: class="slide" -->
- Versement des données dans le guichet de versement
- Enrichissement/Validation des données par les métadonnées RMéS
- Stockage au format parquet dans S3
