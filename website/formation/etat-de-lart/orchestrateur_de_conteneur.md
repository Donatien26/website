# Orchestrateur de conteneurs

<!-- .slide: class="slide" -->

---

## C'est quoi ?

<!-- .slide: class="slide" -->

- Un moyen d'industrialiser l'utilisation du conteneur
- Un outil qui va automatiser le déploiement, la gestion, la mise à l'échelle et la mise en réseau des conteneurs (et plein d'autres choses).

---

## Comment ça marche ?

<!-- .slide: class="slide" -->

![docker](img/docker.gif)

---

<!-- .slide: class="slide" -->

- ❌ Non industrialisable
- ❌ Monitoring
- ❌ Montée en charge ?

---

<!-- .slide: class="slide" -->

![orchestrateur](img/orchestrateur.gif)

---

<!-- .slide: class="slide" -->

- ✔️ Haute disponibilité
- ✔️ Monitoring
- ✔️ Service Discovery
- ✔️ Gestion du trafic réseau
- ✔️ Scalabilité horizontale et verticale
- ✔️ Provisionning
- ✔️ Placement des conteneurs

---

## Panorama de l'existant

<!-- .slide: class="slide" -->

- Kubernetes (le leader)
- Mesos/Marathon (Deprecated Septembre 2019)
- Docker Swarm
