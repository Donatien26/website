# Conteneurisation

<!-- .slide: class="slide" -->

---

## Quid du conteneur runtime ?

<!-- .slide: class="slide" -->

![conteneur-runtime](img/conteneur-runtime.drawio.png)

- OS + Un ensemble de processus
- Cgroups: Restreindre l'accès a certaines ressource
- Namespace: Isolation des processus (communication avec les autres process) + changement le propriétaire du process

---

## Quid de l'image ?

<!-- .slide: class="slide" -->

![conteneur-image](img/conteneur-image.drawio.png)

- C'est un binaire contenant un ensemble de data
- Notion de parent child (ou de couches)
- Partage d'image très simple (une image = une portion de la branche)
- Bug sur une partie de la branche ? On change juste la partie de la partie de la branche
- Une image c'est l'équivalent du moule en pâtisserie

---

## Quid du conteneur ?

<!-- .slide: class="slide" -->

![conteneur-environnement](img/conteneur-environnement.drawio.png)

- La CLI demande le déploiement du conteneur
- Le registry cache va chercher l'ensemble des images nécéssaires à notre conteneur
- Le deamon démarre le conteneur avec l'ensemble des images récupérées
- Si besoin de volumes, modification des cgroups/namespaces
- Un conteneur c'est l'équivalent du gâteau en pâtisserie (on a utilisé le moule = image)
