# OpenSourcer mon application

<!-- .slide: class="slide" -->

---

## Ça veut dire quoi opensourcer ?

- Rendre disponible son appli sur un dépôt public et accessible à tous.
- Être non adhérent à des spécifités Insee :
  - composant libres:
    - BDD
    - Sécurité
  - composant propriétaires:
    - InseeConfig
    - SPOC
    - IGESA
    - autres...

---

## Les risques

<!-- .slide: class="slide" -->

---

### Pour la sécurité

<!-- .slide: class="slide" -->

- Code exposé et exploitation des failles applicatives
- Adhérences fortes aux environnements internes
- Problématiques de gestion de version (ex: Git Ransomware Attack)

---

### Pour l'entreprise

<!-- .slide: class="slide" -->

- Démystification du travail, erreurs, image de l'entreprise
- Compétences spécifiques pour les développeurs
- Problématiques juridiques : droit des données

---

## Quels avantages ?

<!-- .slide: class="slide" -->

- Portabilité, interopérabilité, reproductibilité
- Exécution en externe pour développement hors réseau Insee (PC
  perso en télétravail, prestation externe...)
- Amélioration de la qualité du code
- Facilité du développement et de la maintenance
- ⚠️ Contraintes légales : loi pour une République numérique de 2016,
  publication en opensource par défaut
- Plus d'info [ici](https://inseefr.gitlab.io/open-source/site-documentaire/)

---

## Comment on fait ?

<!-- .slide: class="slide" -->

![appli](img/appli.drawio.png)

---

## Avant toute chose

<!-- .slide: class="slide" -->

- Demander à sa maitrise d'ouvrage l'accord (et contacter la DIIT pour accompagnement)
- Documenter son application (si possible en anglais)
- Choisir une licence [choosealicence.com](https://www.choosealicence.com)
- Un fichier CONTRIBUTING.md résumant le fonctionnement pour contribuer (gestion du workflow git etc...)

---

## Rendre son appli fonctionnel en dehors du réseau Insee

<!-- .slide: class="slide" -->

- Etre capable de surcharger les properties (sans insee-config 🤮)
- postgres ➡️ Utilisation d'un postgres local
- keycloak ➡️ Utilisation d'un keycloak local
- ldap ➡️ Utilisation d'un ldap local
- igesa, spoc ➡️ 😭

---

### InverseOfControl (IOC)

<!-- .slide: class="slide" -->

Aussi appelé injection de dépendances

---

### Un exemple (1/2)

<!-- .slide: class="slide" -->

```java
public class MyClass {

    private SpocSender sender;

    public MyClass() {
        this.sender = new SpocSender();
    }
}
```

➡️ Dépendance entre MyClass et SpocSender pas facile d'avoir plusieurs implémentations de SpocSender

---

### Un exemple (2/2)

<!-- .slide: class="slide" -->

```java
public class MyClass {

    private ISender sender;

    public MyClass(ISender sender) {
        this.sender = sender;
    }
}

MyClass(new SpocSender())

```

➡️ Injection par le constructeur. On peut désormais utiliser plusieurs Sender

---

<!-- .slide: class="slide" -->

- Séparation du quoi faire et du comment le faire
- S'assurer que chaque partie est indépendante des autres.

➡️ Une solution technique spring (Spring).

---

### Spring et IOC

<!-- .slide: class="slide" -->

Spring permet de définir un contexte initialisé au lancement de l'application,
pour ensuite injecter à chaque besoin, le composant déjà instancié.

![spring](img/spring.drawio.png)

---

### Quelques annotations utiles en spring

<!-- .slide: class="slide" -->

➡️ Par défaut un seul bean de chaque type

- `@ConditionnalOnMissingBean`
- `@ConditionalOnSingleCandidate`
- `@Primary`
- `@Qualifier` [exemple](https://zetcode.com/springboot/qualifier/)
- `@ConditionalOnProperty`

---

### Conclusion

<!-- .slide: class="slide" -->

➡️ J'utilise une bdd postgrès en local, un keycloak en local, et j'utilise l'IOC pour injecter différent bean (implémentation de igesa) en fonction de mes besoins.

➡️ J'ai une application qui démarre en local mais qui nécessite la configuration d'un keycloak, d'un postgres, et j'ai toujours l'adhérence avec igesa dans mon module opensource

![appli2](img/appli2.drawio.png)

---

## Séparation en modules

<!-- .slide: class="slide" -->

- Création d'un module opensource avec le code non Insee.
- Création du module Insee avec les adhérences Insee.

---

## Limiter les outils externes

<!-- .slide: class="slide" -->

- Utilisation d'une base de données H2, avec jeu de données injecté en base lors du démarrage de l'appli.
- Utilisation de l'authentification basique à la place de keycloak (activation par properties)

---

<!-- .slide: class="slide" -->

![appli3](img/appli3.drawio.png)

---

## TP

<!-- .slide: class="slide" -->

- Rendre l'application originel opensource (non adhérente a l'environnement insee)
  ➡️ Utilisation base h2
  ➡️ Passage de keycloak à la librairie springsecurity
  ➡️ Multiple implémentation du IgesaService
  ➡️ mettre un ci qui build l'application

---

## Quelques point de vigilance

<!-- .slide: class="slide" -->

🎉 Félicitation votre application est presque opensource.

- ⚠️ reverse engineering (si présence de donnée de tests)
- ⚠️ historique existant Cleaner son répértoire [https ://rtyley.github.io/bfg-
  repo-cleaner/](https ://rtyley.github.io/bfg-
  repo-cleaner/) (Solution plus radicale --> on delete le .git et on recommence)

---

## Intégrer une application opensource (dans appli-insee)

<!-- .slide: class="slide" -->

- ⚠️ par défaut spring fait un fatJar (conteneur de servlet + classes) on a besoin d'un jar
- mvn install du projet opensource sur le poste local
- mettre en dépendance le projet opensource dans projet insee

---

## Application insee

<!-- .slide: class="slide" -->

Partie insee:
➡️ [https://gitlab.insee.fr/ystfg5/appli-insee](https://gitlab.insee.fr/ystfg5/appli-insee)

```
- git clone https://gitlab.insee.fr/ystfg5/appli-insee.git
- git checkout step1-add-opensource-dependencies

```

➡️ l'appli est volontairement une coquille vide, elle ne tourne même pas 😇

---

## TP

<!-- .slide: class="slide" -->

- Récupérer le module insee, lui ajouter le module opensource indépendance, implémenter la surcouche insee (implémentation igesa)

---

## Et dans du CI ?

<!-- .slide: class="slide" -->

Utilisation de git-submodules

---

### C'est quoi un submodule ?

<!-- .slide: class="slide" -->

- Utilisation d'un autre projet comme dépendance
- Vouloir travailler sur 2 projets en même temps.
- Gérer deux projets séparés tout en utilisant l’un dans l’autre.

-> Les sous-modules vous permettent de gérer un dépôt Git comme un sous-répertoire d’un autre dépôt Git.

---

### Mise en place d'un submodule

<!-- .slide: class="slide" -->

`git submodule add https://github.com/link/to/repo`

---

### Et dans mon CI ?

<!-- .slide: class="slide" -->

```yaml
package:java:
  stage: build
  tags:
    - maven-jdk11
  script:
    - git submodule init
    - git submodule update
    - git submodule foreach git reset --hard HEAD
    - git submodule foreach git clean -f -d
    - git submodule foreach git pull origin main
    - mvn install -f appli-opensource
    - mvn package
  artifacts:
    untracked: true
    expire_in: 1 hour
    paths:
      - "./"
```

---

## TP

<!-- .slide: class="slide" -->

- Mettre en place le ci/cd dans le module insee
