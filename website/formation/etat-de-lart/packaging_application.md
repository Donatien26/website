# Packager une application

<!-- .slide: class="slide" -->

- Le packaging s'adapte à la technologie d'architecture utilisée

---

## JAR

<!-- .slide: class="slide" -->

- Java ARchive
- Un "zip" contenant les définitions des classes, ainsi que des métadonnées, constituant l'ensemble d'un programme
- Nécessite une machine avec Java

![jar](img/jar.drawio.png)

---

## WAR

<!-- .slide: class="slide" -->

- Web application ARchive
- Un "zip" contenant l'ensemble des fichiers du jar + jsp, html, javascript, nécessaire pour le fonctionnement de l'application.
- Est déployable sur n'importe quel conteneur de servlet (Tomcat, JBoss, Jetty).
- Nécessite une machine avec java + un conteneur de servlet

![war](img/war.drawio.png)

---

## Uber/Fat JAR (Jar springboot)

<!-- .slide: class="slide" -->

- Java ARchive
- Embarque son propre moteur de servlets
- Se lance en faisant java -jar myapp.jar
- Nécessite une machine avec Java

![fatjar](img/fatjar.drawio.png)

---

## Plus d'infos

<!-- .slide: class="slide" -->

[https://developers.redhat.com/blog/2017/08/24/the-skinny-on-fat-thin-hollow-and-uber/](https://developers.redhat.com/blog/2017/08/24/the-skinny-on-fat-thin-hollow-and-uber/)

---

## Le conteneur

<!-- .slide: class="slide" -->

- Un paquet contenant l'application ainsi que toutes les dépendances nécessaires pour le bon fonctionnement de l'appli
- Indépendant de l'appli qui tourne dans le conteneur
- Indépendant de la plateforme sur laquelle le paquet est déployé
- Nécessite une machine avec un moteur de conteneur (peu importe ce qu'il y a dans le conteneur)

![container](img/container.drawio.png)
