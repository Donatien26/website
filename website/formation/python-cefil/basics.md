# Introduction

<!-- .slide: class="slide" -->

---

## Et si on se présentait ?

<!-- .slide: class="slide" -->

---

## Programme

<!-- .slide: class="slide" -->

- Découverte de Python et de l'environnement du développeur
- Hello Python
- Mes premiers scripts Python
- Mes premières fonctions Python
- Programmation orientée objet avec Python
- Bonnes pratiques de code
- Versionner son code
- Mobilité et travail à l'Insee 

---

## C'est quoi ?

- Un language de programmation
- Créé en 1989 par Guido van Rossum, aux Pays-Bas
- 2 Versions majeures cohabitent Python 2 (obsolète) et Python 3
- Version actuelle de Python : 3.13 (3.14 en janvier)

---

## Pourquoi Python ?

<!-- .slide: class="slide" -->

- Il est libre, opensource et gratuit
- Il fonctionne sur tous les OS (Windows, Linux, Mac)
- C'est un langage de programmation dont la syntaxe est plutôt simple
- Il est à la mode
- Il est orienté objet
- Facilement extensible (ajout de librairie/module externe)
- Un langage de programmation adapté pour:
  - Du développement web
  - De la data science
  - Des jeux vidéos
  - Et plein d'autres "use cases"

---

## Python landscape

<!-- .slide: class="slide" -->

![python landscape](assets/python_landscape.png)

---

## Python classement

<!-- .slide: class="slide" -->

![python ranking](assets/language-ranking.png)

---

## Présentation des outils

<!-- .slide: class="slide" -->

- Python
- Onyxia [datalab.sspcloud.fr](datalab.sspcloud.fr)
- VScode
- Git

---

## Onyxia

<!-- .slide: class="slide" -->

- Une plateforme web opensource créé par l'Insee à destination des agents des ministères.
- Mise à disposition d'environnements pour se former sur de nouvelles technologies (Python, R, Bigdata,...)
- Aucune garantie de service, on n'y met pas de données sensibles !!
- On sauvegarde son travail régulièrement sur Git (le code) et minio (les données)

---

## Démonstration

<!-- .slide: class="slide" -->

[datalab.sspcloud.fr](datalab.sspcloud.fr)
![datalab.sspcloud.fr](assets/datalab-sspcloud.PNG)

---

## VSCODE

<!-- .slide: class="slide" -->

- Un IDE (ou environnement de développement intégré) : logiciel de création d'applications, qui rassemble des outils de développement fréquemment utilisés dans une seule interface utilisateur graphique
- Pourquoi VScode ?
  - Simple d'utilisation
  - Rassemble tous les outils
  - Utilisable pour n'importe quel langage (Python, Java, HTML, JavaScript, R,...)

---

## VSCODE

 <!-- .slide: class="slide" -->

- Démonstration

---

## Git

 <!-- .slide: class="slide" -->

- On verra plus en détail à la fin

---

## TP0

 <!-- .slide: class="slide" -->

-> [TP0](/website/formation/python-cefil/Tps/tp0.html)
