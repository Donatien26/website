from tp4.metier.pokemon import Pokemon
from tp4.metier.dresseur import Dresseur


def combat(pokemon_1: Pokemon, pokemon_2: Pokemon):
    while (pokemon_1.vie > 0 and pokemon_2.vie > 0):
        pokemon_1.attaque(pokemon_2)
        if pokemon_2.vie > 0:
            pokemon_2.attaque(pokemon_1)
    if pokemon_1.vie == 0:
        print(pokemon_2.nom + " a gagné")
    else:
        print("{} a gagné".format(pokemon_1.nom))


def duel(dresseur1: Dresseur, dresseur2: Dresseur):
    """[summary]

    Args:
        dresseur1 (Dresseur): [description]
        dresseur2 (Dresseur): [description]
    """
    while (len(dresseur1.pokemons) > 0 and len(dresseur2.pokemons) > 0):
        pokemon_1 = dresseur1.pokemons[0]
        pokemon_2 = dresseur2.pokemons[0]
        combat(pokemon_1, pokemon_2)
        if pokemon_1.vie <= 0:
            dresseur1.pokemons.pop(0)
        else:
            dresseur2.pokemons.pop(0)
    if len(dresseur1.pokemons) == 0:
        print(dresseur1.nom + " a gagné")
    else:
        print("{} a gagné".format(dresseur2.nom))