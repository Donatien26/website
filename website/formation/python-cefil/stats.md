# Analyse statistique avec python

<!-- .slide: class="slide" -->

---

## Pandas (1/3)

<!-- .slide: class="slide" -->

Le package pandas est l’une des briques centrales de l’écosystème de la data-science.

- Créé par Wes McKinney.
- Surcouche a la librairie `numpy`
- Introduit la notion de dataframe

---

## Logique de pandas

<!-- .slide: class="slide" -->

L’objet central dans la logique `pandas` est le `DataFrame`.
Il s’agit d’une structure particulière de données
à deux dimensions, structurées en alignant des lignes et colonnes. Les colonnes
peuvent être de type différent.

Un `DataFrame` est composé des éléments suivants:

- l’indice de la ligne ;
- le nom de la colonne ;
- la valeur de la donnée ;

Structuration d’un DataFrame pandas,
empruntée à <https://medium.com/epfl-extension-school/selecting-data-from-a-pandas-dataframe-53917dc39953>:

---

## Valeurs manquantes

<!-- .slide: class="slide" -->

- Les valeurs manquantes sont affichées `Nan`

---

## Jupyter notebook

Un outil dédié pour faire de l'analyse statistique

Démo -> https://datalab.sspcloud.fr/catalog/ide

---

## Lien annexe

- L’[aide officielle de pandas](https://pandas.pydata.org/docs/user_guide/index.html).
  Notamment, la
  [page de comparaison des langages](https://pandas.pydata.org/pandas-docs/stable/getting_started/comparison/index.html)
  est très utile
- La _cheatsheet suivante_, [issue de ce post](https://becominghuman.ai/cheat-sheets-for-ai-neural-networks-machine-learning-deep-learning-big-data-678c51b4b463)

![Cheasheet pandas](https://cdn-images-1.medium.com/max/2000/1*YhTbz8b8Svi22wNVvqzneg.jpeg)

---

## TP4

 <!-- .slide: class="slide" -->

-> On va utiliser les cours d'Onyxia: https://www.sspcloud.fr/formation?search=&path=%5B%22Python%20pour%20la%20data%20science%22%2C%22Manipulation%20de%20donn%C3%A9es%22%5D
