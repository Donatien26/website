# Quelques bonnes pratiques

<!-- .slide: class="slide" -->

---

## Documentation

---

### Objectif

<!-- .slide: class="slide" -->

La documentation est partie intégrante du métier de développeur.

Elle a pour but de :

- faciliter la compréhesion du code
- faciliter la maintenance

---

### Comment ?

<!-- .slide: class="slide" -->

```python

def ma_fonction(argument1, argument2):
    '''Description courte

    Description longue

    Parameters
    ----------
    argument1: type1
        description de argument G
    argument2: type2
        description de argument H

    Returns
    -------
    type de retour
        description valeur retournée

    Examples
    --------
    >>> ma_fonction(1, 2)
    'bla bla'
    '''
    operation1
    operation2
    return resultat

```

On remplace summary par ce que fait la fonction

Args correspond aux paramètres pris par votre fonction. On trouve entre parenthèses le type de l'objet et une description

---

<!-- .slide: class="slide" -->

Tout documenter n'est pas forcément utile, commentez vos fonctions et vos classes qui vous semblent les plus techniques ou compliquées à comprendre

---

## Nom variables/fonction

<!-- .slide: class="slide" -->

---

### Exemple

```python
d:int=5

elapsedTimeInDays:int=5

```

---

### Exemple

<!-- .slide: class="slide" -->

Vous préfèrez :

```python
def applies(e:Event):
    if (e.d == 6 or e.d == 0):
         return False
    else:
        return (e.t.h >= 8 and e.t.h < 18)
}

```

---

<!-- .slide: class="slide" -->

ou :

```python
def isDuringWorkingHours(event: Event):
    if (event.dayOfWeek == DayOfWeek.SATURDAY or event.dayOfWeek == DayOfWeek.SUNDAY):
         return False
    else:
        return (event.time.hour >= WORKING_DAY_START && event.time.hour < WORKING_DAY_END)
}


```

---

### Quelques conventions

<!-- .slide: class="slide" -->

Nom des variables en snakecase

Nom des classes en camelcase

Constante en snakecase + camelcase

Nom des packages en snakecase

---

## Séparation module/package (1/2)

<!-- .slide: class="slide" -->

En programmation, on essaie d'organiser le code de nos applications. Le but étant de faciliter la lisibilité et la maintenance du code. On essaie au maximun de respecter les règles suivantes:

- Une classe = un fichier = un module
- Un ensemble de fonctions = un fichier = un module
- Un dossier = un ensemble de classes cohérentes = un package

---

## Séparation module/package (2/2)

<!-- .slide: class="slide" -->

Pour importer un module dans un autre module il faut rajouter en haut du module la ligne suivante :

```python
from chemin.vers.le.module import ceQueVousVoulezImporter

```

---

## Installer des dépendances externes (1/3)

<!-- .slide: class="slide" -->

Python permet d'installer des bibliothèques externes (package)

Pour cela il propose un gestionnaire de paquets "**pip**". "**pip**" est directement installé avec Python. Une partie des packages est disponible ici : https://pypi.org/

Vous entendrez sûrement parler des packages suivants: **pandas**, **NumPy**, **Requests**,

---

## Installer des dépendances externes (2/3)

<!-- .slide: class="slide" -->

Pour installer une dépendance externe, il suffit d'écrire pip install nom_de_la_dependance

```bash
pip install requests

```

---

## Installer des dépendances externes (3/3)

<!-- .slide: class="slide" -->

⚠️ Il est conseillé de regrouper l'ensemble des dépendances utilisées dans le projet dans un fichier **requirements.txt**. Par exemple :

```
appnope==0.1.0
backcall==0.1.0
beautifulsoup4==4.6.3
```

Un utilisateur qui récupère votre code saura donc quelle dépendance et quelle version installer.

---

## Tests unitaires

<!-- .slide: class="slide" -->

---

## Motivations

<!-- .slide: class="slide" -->

Déboggage :

- 50% du temps d’un expert
- 90% du temps d’un débutant
  Problème de reprise de code :
- Comment fonctionne cet algorithme?
- Qu’est-ce qui fonctionne et qu’est-ce qui ne fonctionne pas?

---

### Objectif

<!-- .slide: class="slide" -->

**Vérifier que votre code fonctionne comme vous le souhaitez** : réduire le temps de développement, s’assurer que la fonction ou la méthode réagit correctement (bons résultats, erreur si on sort du cadre de sa fonction), éviter de chercher un bug pendant des heures

**Faciliter la maintenance** : s’assurer qu’un bug précédent ne se reproduise pas (lors d'une modification d'un programme, les tests unitaires signalent les éventuelles régressions)

**Documenter le code** : donner toutes les spécifications que l’on souhaite, servir de démonstration aux autres développeurs et aux utilisateurs

---

### Comment on fait ? (1/?)

<!-- .slide: class="slide" -->

Plusieurs frameworks de développement de tests unitaires pour Python :

- doctest
- unittest
- nose
- pytest

Nous allons utiliser unittest appelé également PyUnit.

---

### Comment on fait ? (2/?)

<!-- .slide: class="slide" -->

Création d’un test via le module unittest :

- 1/ On dérive la classe unittest.TestCase
- 2/ On crée une ou plusieurs méthodes dont le nom commence par
     test à notre classe

```python
import unittest
from service.operation import moyenne

class OperationTest(unittest.TestCase):
    def test_choice(self):
        liste = [1,2,3,4]
        moy = moyenne(liste)
        # Vérifions que 'elt'est dans 'liste':
        self.assertEqual(moy, 2.5)

>>> unittest.main()
```

---
