---
title: "TP2 : Les fonctions"
abstract: "Création de fonctions python, découverte de la notion de variables globale et locale."
---

Dans ce troisième TP, nous découvrirons l'intérêt de réaliser des fonctions ainsi que comment les déclarer en Python. Nous allons également aborder la notion de variable globale versus la notion de variable locale.

## Les fonctions

### Definition\*

Dès que l’on écrit un morceau de code nécessitant une réutilisation ultérieure, ou une documentation, la création d’une fonction devient intéressante, voire obligée. Pour cela, en Python, rien de plus simple :

```python
def ma_fonction(argumentG, argumentH) :
    operationG
    operationH
    return resultat
```

Plusieurs points sont à noter. Tout d’abord, on commence par le mot-clé **def** indiquant le début d’une définition de fonction. Vient ensuite le nom que l’on a choisit pour notre fonction. Puis, entre parenthèses, la liste des noms des arguments (i.e. notre choix de noms de variables pour les arguments passés à notre fonction lorsqu’elle est appelée). Finalement, la première ligne de définition se termine par un deux-points. Ensuite, on retrouve la documentation de la fonction, et enfin le corps de celle-ci. Pour retourner une valeur, il suffit d’utiliser le mot-clé **return**.

### Principe du Don't Repeat Yourself

Les fonctions permettent de satisfaire ce principe en évitant la duplication de code. En effet, plus un code est dupliqué plusieurs fois dans un programme, plus il sera source d'erreurs, notamment lorsqu'il faudra le faire évoluer.

Considérons par exemple le code suivant qui convertit une certaines sommes d'euros en dollars :

```python
somme_euros=1
somme_euros*1,1249
>> 1,1249


...


somme_euros=10
somme_euros*1,1249
>> 11,249


```

Il faut alors reprendre toutes les lignes précédentes et les corriger. Cela n'est pas efficace, surtout si le même code est utilisé à différents endroits dans le programme.

En écrivant qu'une seule fois la formule de conversion dans une fonction, on applique le principe DRY :

```python

def convert_euros_en_dollars(somme):
    return somme*1,1249

convert_euros_en_dollars(1)
>> 1,1249

```

Et s'il y a une erreur dans la formule, il suffira de la corriger qu'une seule fois, dans la fonction convert_euros_en_dollars()

### Exemple

Fonction qui ne renvoie rien

```python
def afficher_text(text):
    print(text)

afficher_text("hello")
>> "hello"
```

Fonction à plusieurs paramètres

```python
def multiplier(x,y):
    return x*y

a=mulitplier(2,3)
print(a)
>>6
```

Fonction retournant un booléen

```python
def est_entier(x):
    if (x==int(x)):
        return True
    else:
        return False

est_entier(3)
>> True

est_entier("eiejieij")
>> False
```

## Variables globales vs variables locales

Imaginez que vous collaborez pour écrire un très, très long programme. Si je définis une variable "longueur" égale à 32 et vous définissez la même variable avec le même nom ailleurs avec la valeur 45, nous risquons d’avoir un problème en utilisant ces variables.

Python et la plupart des langages de programmation ont une solution pour ce genre de problème : les variables qui sont définies à l’intérieur d’une fonction ne sont connues qu’à l’intérieur de cette fonction. Donc si vous définissez une variable "longueur" et que je définis dans une autre fonction une variable "longueur", celles-ci seront traitées comme deux variables différentes par Python.

On dira que ces variables sont locales à la fonction ou qu’elles ont une portée locale.

A l'inverse une variable commune à l'ensemble du programme est une variable dite globale.

De manière générale, une variable déclarée dans une fonction est une variable locale, une variable déclarée en dehors des fonctions est globale. Si 2 variables avec le même nom sont déclarées à la fois dans une fonction et en dehors, alors c'est la variable de la fonction qui est prise en compte.

## Questions de compréhension

- 1/ Pourquoi dit-on que l'utilisation de fonctions dans un programme est une bonne pratique de développement ?
- 2/ Quelles sont les trois caractéristiques d'une fonction ?
- 3/ Qu'est-ce qu'une fonction "boîte noire" ? A quelles autres fonctions s'oppose-t-elle ?
- 4/ Que se passe-t-il quand on définit une fonction ? Et quand on l'appelle ?
- 5/ Combien d'arguments peut-on passer à une fonction ?
- 6/ Quelles sont les deux modalités de passage d'arguments à une fonction ?
- 7/ Quelle est l'utilité de passer des arguments optionnels à une fonction ?
- 8/ Dans quel ordre doivent être passés les arguments d'une fonction si celle-ci a à la fois des arguments obligatoires et optionnels ?
- 9/ Existe-il des fonctions qui ne renvoient rien ?
- 10/ Une fonction peut-elle renvoyer plusieurs objets ?
- 11/ Que deviennent les variables du _scope_ local d'une fonction une fois que la fonction a été appelée ?

## Exercices

### Exercice 1

Ecrivez une fonction puissance qui prendra en paramètre 2 entiers x et y, et qui renverra x à la puissance y.

**rappel** :

```python
2**2
> 4

10**2
> 100

```

### Exercice 2

Reprenez l'exo du TP1 permettant de calculer une moyenne à partir d'une liste. Faites en sorte d'avoir une fonction qui prend n'importe quelle liste en entrée et qui renverra la moyenne des éléments de la liste.

### Exercice 3

Considérons la fonction ci-dessous :

```python

def ma_fonction(list):
    list_copy=list
    list_copy[0]="toto"
    return list_copy

l1=["tata","tutu"]
l2=ma_fonction(l1)

print(l1)
print(l2)

```

- Selon vous qu'allons nous obtenir ?
- Implémenter et vérifier votre résultat
- Expliquez

### Exercice 4 : Sauvegarder votre travail

```bash
git checkout -b tp2
git add .
git commit -m "fin tp2"
git push
```
