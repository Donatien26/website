---
title: "TP0 : Mise en place de l'environnement"
abstract: "Découvrir l'environnement"
---

Dans ce premier TP, nous allons mettre en place votre environnement afin que vous puissiez par la suite réaliser les prochains TPs.

## Exercice

### Exercice 0 : Création de l'environnement de code

- Se rendre sur [https://datalab.sspcloud.fr/home](https://datalab.sspcloud.fr/home)
- Dans la barre latérale, cliquer sur catalogue de services
- Dans la liste chercher VScode, et cliquer lancer (normalement on devrait vous demander de vous authentifier)
- Dans l'onglet security décocher enable IP protection
- Cliquer lancer
- Une fois le service prêt, n'oublier pas de récupérer votre mot de passe et faites ouvrir

### Exercice 1

Découvrir VScode :

- faites apparaitre le terminal
- créer un dossier
- créer un fichier (.txt) dans ce dossier

### Exercice 2 : Récupération du dépôt Git

- Connexion [gitlab](https://gitlab.com/users/sign_in)
- Forker le projet [python_tp_cefil](https://gitlab.com/Donatien26/python_tp_cefil)
  ![datalab.sspcloud.fr](../assets/fork.png)
  :bulb: Un fork est une copie totale d'un dépôt ne vous appartenant pas. En le forkant vous le copier dans la liste de vos dépôts et vous avez tous les droits sur ce nouveau dépôt.

- cloner votre dépôt:
  - cliquer sur le bouton bleu et copier la ligne clone with https
    ![datalab.sspcloud.fr](../assets/clone.png)
  - retourner dans VScode et écrire les lignes suivantes:
  ```bash
  coder@vscode-826743-5dd685b898-ln2sw:~/work$ git clone https://gitlab.com/Donatien26/python_tp_cefil.git
  >>> Cloning into 'python_tp_cefil'...
      remote: Enumerating objects: 3, done.
      remote: Counting objects: 100% (3/3), done.
      remote: Compressing objects: 100% (2/2), done.
      remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
      Receiving objects: 100% (3/3), done.
  coder@vscode-826743-5dd685b898-ln2sw:~/work$ cd python_tp_cefil/
  ```

### Exercice 3 : Mon premier programme Python

 <!-- .slide: class="slide" -->

Au sein du terminal VScode :

- Écrivez "python" et vous devriez voir des informations à propos de Python apparaître avec >>> indiquant où écrire le code.
- Ensuite, écrivez "print("hello, world!")" .
- Appuyez sur Entrée pour voir le résultat.

:tada: Vous venez de faire tourner votre premier programme Python.

#### Que vient on de faire ?

 <!-- .slide: class="slide" -->

Démarrer une console Python (un endroit où on peut donner des instructions à Python), et on lui a demandé d'afficher `hello world !` avec la très pratique et très utilisée fonction `print`. Elle affiche simplement l'argument qu'on lui passe entre parenthèses **et** un retour à la ligne par défaut.

Vous pourrier donner d'autres instructions comme :

- Additionner, multiplier, diviser, soustraire 2 nombre
- Formatter des chaines de caractères

Mais ce n'est pas très pratique 😭

### Exercice 4 : Mon premier script Python

Au sein de VScode :

- Créer un fichier python (.py)
- Ensuite, écrivez "print("hello, world!")"
- Lancer votre fichier

:tada: Vous venez de faire tourner votre premier script Python.

### Exercice 5 : Sauvegarder votre travail

```bash
git checkout -b tp0
git add .
git commit -m "fin tp0"
git push
```
