---
title: "TP1 : Types de base, variables, condition et boucle"
abstract: "Manipulation des nombres, chaînes de caractères et des variables"
---

Dans ce second TP, nous allons découvrir les objets fondamentaux de Python : les **nombres**, les **chaînes de caractères**, et les **listes**. Nous allons voir comment l'on peut assigner des objets à des **variables**, afin de réaliser des opérations avec ces objets. Enfin nous découvrirons comment réaliser des boucles et des conditions.

## Nombres

### Types de nombres

Python propose différents objets de type numérique. Dans ce tutoriel, on va s'intéresser aux deux types principalement utilisés :

- les entiers (type `int` pour _integer_)
- les réels (type `float` pour nombres à virgule flottante)

De manière générale, on utilise la fonction `type` pour imprimer le type d'un objet Python.

```python
type(3)
```

```python
type(3.14)
```

Les fonctions `float` et `int` peuvent être utilisées pour passer d'un type à l'autre.

```python
# Conversion en float
float(3)
```

```python
# Conversion en float
type(float(3))
```

```python
# Conversion en int
int(3.79)
```

Attention à la conversion _float_ -> _int_, qui tronque la partie décimale.

Les _floats_ peuvent par ailleurs être écrits en notation scientifique :

```python
2e3
```

```python
type(2e3)
```

### Opérations arithmétiques de base

```python
# Addition
8 + 9
```

```python
# Soustraction
5 - 2
```

```python
# Multiplication
2 * 6
```

```python
# Division
9 / 4
```

```python
# Division par 0
3 / 0
```

C'était bien sûr prévisible. Mais il n'est pas rare d'avoir de telles erreurs dans le cadre de calculs statistiques, notamment avec _NumPy_ ou _Pandas_, produisant une erreur similaire qu'il faut alors débugger.

```python
# Division euclidienne : quotient
9 // 4
```

```python
# Division euclidienne : reste
9 % 4
```

```python
# Puissance
2 ** 5
```

```python
# Racine carrée
5 ** 0.5
```

```python
# Ordre des opérations : convention usuelle
2 + 5 * (10 - 4)
```

## Chaînes de charactères

Les chaînes de caractères (ou _strings_) sont utilisées pour stocker de l'information textuelle. Plus précisément, elles peuvent stocker tout caractère de type [Unicode](https://fr.wikipedia.org/wiki/Unicode), ce qui inclut les lettres des différentes langues, mais également la ponctuation, les chiffres, les smileys, etc.

Un _string_ se définit en mettant l'information entre apostrophes ou entre guillemets (anglais).

### Définition

```python
# Première manière
'mot'
```

```python
# Deuxième manière
"ça fonctionne aussi"
```

```python
# Mais attention au mélange des deux !
'l'apostrophe, quelle catastrophe'
```

Erreur de syntaxe : la seconde apostrophe est compris comme la fin du _string_ et Python ne sait pas interpréter le reste de la séquence.

Il faut donc varier en cas de besoin :

```python
"l'apostrophe, aucun problème"
```

Même chose en sens inverse :

```python
'les guillemets, "aucun problème"'
```

### Longueur d'une chaîne

La fonction `len` permet de compter le nombre de caractères d'un _string_, tous caractères inclus (lettres, chiffres, espaces, ponctuation...).

```python
len("J'ai 19 charactères")
```

Le type "caractère" n'existe pas en Python : un caractère seul est défini comme un _string_ de taille 1.

```python
print(type("a"))
print(len("a"))
```

### Indexation

En Python, un _string_ est une **séquence**, c'est à dire une suite de caractères dans un ordre spécifique. Par conséquent, chaque caractère d'un _string_ est indexé (Python connaît sa position), et l'on peut utiliser cet index pour extraire des caractères particuliers, des sous-chaînes de caractères, etc.

En Python, on utilise les crochets `[]` pour appeler l'index d'une séquence. Plus précisément, l'index fonctionne sur le modèle suivant : `x[a:b:c]` renvoie un _sub-string_ du *strin*g `x` où `a` est la position du caractère de départ, `b` la position du caractère d'arrivée, et `c` le pas de l'indexation. Tout cela sera plus clair avec les exemples suivants.

Note importante : **l'indexation commence à 0 en Python**.

```python
"une séquence que l'on va indexer"
```

```python
# Premier élémént
"une séquence que l'on va indexer"[0]
```

```python
# Deuxième élémént
"une séquence que l'on va indexer"[1]
```

```python
# Dernier élémént
"une séquence que l'on va indexer"[-1]
```

```python
# Extraire tout à partir d'un certain caractère
"une séquence que l'on va indexer"[4:]
```

```python
# Extraire tout jusqu'à un certain caractère
"une séquence que l'on va indexer"[:12]
```

```python
# Extraire un sub-string
"une séquence que l'on va indexer"[4:12]
```

```python
# Extraire tous les 2 caractères, à partir de la 4 ème position
"une séquence que l'on va indexer"[4::2]
```

```python
# Inverser une séquence
"une séquence que l'on va indexer"[::-1]
```

A retenir : c'est bien parce qu'un _string_ est considéré comme une séquence par Python que l'on peut l'indexer. Par exemple, indexer un nombre n'a pas de sens, et renvoie donc une erreur.

```python
2[3]
```

### Quelques propriétés utiles

```python
# Concaténation de strings
"mon adresse est : " + "10 rue des Peupliers"
```

```python
# Répétition
"echo - " * 5
```

### Quelques méthodes utiles

Les différents objets Python ont généralement des **méthodes** dites _built-in_ (standard), qui permettent d'effectuer des opérations de base à partir de l'objet.

Nous verrons dans un prochain chapitre en quoi consistent précisément les méthodes en Python. Pour le moment, on peut retenir que les méthodes s'utilisent selon le format `objet.methode(parametres)` où les paramètres sont optionnels.

```python
# Mettre en majuscules
"sequence 850".upper()
```

```python
# Mettre en minuscules
"sequence 850".lower()
```

```python
# Séparer les mots selon les espaces
"une séquence    à séparer".split()
```

```python
# Séparer les mots selon un caractère arbitraire
"pratique pour faire des sous-séquences".split("-")
```

```python
# Utiliser les strings comme templates
"mon adresse est : {}".format("10 rue des Peupliers")
```

Tout ceci n'est qu'un aperçu des innombrables opérations possibles sur les _strings_. La [documentation officielle](https://docs.python.org/3/library/stdtypes.html#string-methods) liste l'ensemble des méthodes _built-in_ disponibles. Les exercices du chapitre et les mini-projets de fin de partie seront l'occasion de découvrir d'autres utilisations.

## Les listes

### Définitions

Tout comme les chaînes de caractères étaient des séquences de caractères. Les listes sont également des séquences, c'est à dire des suites ordonnées d'éléments, mais plus générales : les éléments peuvent être de différente nature.

Les listes sont construites avec des crochets [], et les éléments de la liste sont séparés par des virgules.

```python
a = [1, 2, 3]
print(a)
```

La liste `a` est constituée d'entiers, mais une liste peut en pratique contenir des objets de tout type.

```python
b = ["une séquence", 56, "d"]
print(b)
```

Il est notamment possible de créer des listes de listes (et ainsi de suite), ce qui permet de créer des structures hiérarchiques de données.

```python
c = ["une séquence", 56, ["cette liste est imbriquée", 75, "o"]]
print(c)
```

Une liste imbriquée peut aussi être construite à partir de listes déjà définies.

```python
item1 = ["cafe", "500g"]
item2 = ["biscuits", "20"]
item3 = ["lait", "1L"]
inventaire = [item1, item2, item3]
print(inventaire)
```

On verra cependant dans le prochain tutoriel que les dictionnaires sont généralement des structures de données souvent plus adaptées que les listes pour représenter des données sous forme hiérarchique.

### Longueur d'une liste

Comme les chaînes de caractères, il est possible d'utiliser la fonction `len` pour compter le nombre d'éléments présents dans une liste.

```python
d = ["ceci", "est", "une", "liste"]
len(d)
```

### Indexation

Les listes étant des séquences, elles s'indexent de la même manière que les chaînes de caractères. Il est notamment important de se rappeler que la numérotation des positions commence à 0 en Python.

```python
# Troisième élément de la liste a
print(a[2])
```

Bien entendu, il n'est pas possible de demander un élément qui n'existe pas. Python renvoie une erreur nous indiquant que l'index demandé est hors limites.

```python
print(a[5])
```

Pour indexer une liste contenue dans une autre liste, on utilise une double indexation.

```python
# Premier élément de la sous-liste qui est à la deuxième position de la liste c
print(c[2][0])
```

En termes d'indexation, tout ce qui était possible sur les chaînes caractères l'est également avec les listes.

```python
# Tous les éléments à partir de la 1ère position
print(b[1:])
```

```python
# Inverser une liste
print(a[::-1])
```

### Modification d'éléments

Il est possible de modifier les éléments d'une liste manuellement, avec une syntaxe similaire à l'assignation de variable.

```python
# Réassignation d'un élément
d = [1, 2, "toto", 4]
d[2] = 3
print(d)
```

```python
# Substitution d'un élément
a = [1, 2, 3]
b = ["do", "re", "mi"]
b[0] = a[2]
print(b)
```

### Suppression d'éléments

L'instruction `del` permet de supprimer un élément par position. Les éléments qui se trouvaient après l'élément supprimé voient donc leur index réduit de 1.

```python
e = [1, "do", 6]
print(e)
print(e[2])

del e[1]
print(e)
print(e[1])
```

### Quelques propriétés utiles

Là encore, on retrouve des propriétés inhérentes aux séquences.

```python
# Concaténation
[1, 2, 3] + ["a", 12]
```

```python
# Réplication
["a", "b", "c"] * 3
```

### Quelques méthodes utiles

A l'instar des chaînes de caractères, les listes ont de nombreuses méthodes _built-in_, qui s'utilisent selon le format `objet.methode(parametres)`. Les plus utiles sont présentées ci-dessous ; d'autres méthodes seront utilisées dans le cadre des exercices de fin de section.

```python
# Ajouter un élément
a = [1, 2, 3]
a.append(4)
print(a)
```

```python
# Supprimer un élément par position
b = ["do", "re", "mi"]
b.pop(0)
print(b)
```

```python
# Supprimer un élément par valeur
b = ["do", "re", "mi"]
b.remove("mi")
print(b)
```

```python
# Inverser une liste
l = [1, 2, 3, 4, 5]
l.reverse()
print(l)
```

```python
# Trouver la position d'un élément
b = ["a", "b", "c", "d", "e"]
b.index("d")
```

## Variables

Jusqu'ici, nous avons dû définir à chaque fois notre objet avant de pouvoir lui appliquer une transformation. Comment faire si l'on veut réutiliser un objet et lui appliquer plusieurs transformations ? Ou faire des opérations à partir de différents objets ?

Pour cela, on va assigner les objets à des variables.

### Assignation et opérations

L'assignation se fait suivant le format : `nom_de_la_variable = objet`. Cela permet ensuite de réaliser des opérations à partir de ces variables.

```python
x = 5
x
```

```python
type(x)
```

```python
x + 5
```

```python
y = x + 2*x
y
```

Contrairement à d'autres langages de programmation, Python est dit _dynamiquement_ typé : il est possible de réassigner une variable à un objet de type différent. Cela facilite la lecture et le développement, mais peut parfois générer des problèmes difficiles à débugger... Il faut donc toujours bien faire attention que le type de la variable est bien celui que l'on s'imagine manipuler.

```python
x = 3
x = "blabla"
type(x)
```

Il y a naturellement certaines contraintes sur les opérations selon les types des objets.

```python
x = "test"
y = 3
x + y
```

Il est par contre possible d'harmoniser les types en amont :

```python
x = "test"
y = 3
z = str(y)
x + z
```

### Incrémentation

Il est fréquent d'utiliser une variable comme un compteur, en l'incrémentant à chaque fois qu'un évènement donné apparaît par exemple.

```python
a = 0
print(a)
a = a +1
print(a)
```

Cette pratique est tellement fréquente qu'il existe des opérateurs spéciaux pour les opérations arithmétiques courantes.

```python
a = 0
a += 1
a
```

```python
b = 5
b *= 3
b
```

## Les tests

### Définition/Exemple

```python

if (condition1):
    # SI condition1 verifié
    deschosesici
elif (condition2):
    # Si condition2 verifié
    deschosesencoreici
elif...:
...
elif... :
else:
    # Si aucune des conditions précédentes n'est vérifiée
    deschosesenfinici

```

### Les comparateurs

<!-- .slide: class="slide" -->

| Syntaxe Python | Signification       |
| :------------- | :------------------ |
| ==             | égal à              |
| !=             | différent de        |
| >              | supérieur à         |
| <=             | supérieur ou égal à |
| <              | inférieur à         |
| >=             | inférieur ou égal à |

## Les boucles

### Boucles for

#### Syntaxe

Analysons la structure d'une boucle `for` :

- La première ligne spécifie une **instruction `for`**, et comme toute instruction en Python se termine par `:`.
- Vient ensuite un **bloc d'instructions**, i.e. une suite d'opérations (une seule dans notre exemple) qui sera exécutée à chaque itération de la boucle. Ce bloc est visible par son **niveau d'indentation**, incrémenté de 1 par rapport à l'instruction. Le bloc s'arrête dès lors que l'indentation revient à son niveau initial.

Comme pour les instructions conditionnelles de type `if`/`else`, l'indentation est donc capitale. Si on l'oublie, Python renvoie une erreur.

```python
for note in gamme:
print(note)
```

#### Fonctionnement

Regardons maintenant plus en détail ce que fait l'instruction `for`. Elle définit une **variable d'itération** (appelée `note` dans notre exemple), qui va parcourir les éléments de l'**itérateur** spécifié après le `in` (la liste `solfege` dans notre exemple). La syntaxe d'une boucle en Python se prête bien à une description littérale ; dans notre cas : "pour chaque note contenue dans la liste solfege, imprime la note".

Insistons sur le fait qu'une boucle définit une variable, sans que l'on ait besoin de passer par la syntaxe traditionnelle d'assignation `variable = valeur`. De plus, cette variable n'est pas supprimée une fois la boucle terminée, elle prend alors la valeur du dernier élément de l'itérateur.

L'itérateur n'est pas nécessairement une liste, il peut être tout objet itérable. Cela inclut notamment tous les objets séquentiels que nous avons vu.

```python
for char in "YMCA":
    print(char)

print()  # Saut de ligne

t = (1, 2, 3, 4, 5)
for i in t:
    print(i*9)
```

### Boucle while

#### Définition

Les boucles `while` fournissent une manière alternative de spécifier des procédures répétitives. L'idée n'est plus d'itérer sur un nombre d'objets fixé à l'avance, mais d'**itérer tant qu'une condition (test logique) est remplie**.

```python
i = 1
while i <= 5:
    print(i)
    i = i + 1
```

#### Syntaxe

La différence essentielle avec la boucle `for` est l'instruction : c'est désormais une instruction `while`, suivie d'une condition (test), et comme toute instruction de `:`.

Pour le reste, le principe est le même : l'instruction `while` est suivi d'un bloc d'instructions, indenté d'un niveau, et qui s'éxécute séquentiellement à chaque itération de la boucle.

#### Critère d'arrêt

Une différence essentielle des boucles `while` par rapport aux boucles `for` tient au critère d'arrêt. Dans une boucle `for`, ce critère est clair : la boucle itère sur les éléments d'un objet itérable, nécessairement de taille finie. La boucle s'arrête donc lorsque chaque élément de l'itérable a été parcouru.

Dans une boucle `while` au contraire, le critère d'arrêt est donné par une condition logique, c'est donc l'utilisateur qui doit fixer le critère d'arrêt. Dans l'exemple, pour que la boucle s'arrête, il faut que la condition `i <= 5` devienne `False`, c'est à dire que `i` devienne strictement supérieur à $5$. On s'est assuré de cela en initialisant `i` à $1$ avant le début de la boucle, puis en incrémentant `i` d'une unité à chaque itération.

Que se passe-t-il si l'on oublie d'incrémenter `i` ? Le critère d'arrêt n'est jamais atteint, la boucle est donc infinie, et il faut utiliser le bouton "Stop" (carré noir) de Jupyter pour arrêter le programme en cours. Vérifions cela en incrémentant la mauvaise variable.

```python
i = 1
j = 1
while i <= 5:
    j = j + 1
```

```python
print(i)
print(j)
```

### Questions de compréhension

- 1/ Comment fonctionne une boucle `for` ?
- 2/ La variable d'itération définie lors d'une boucle `for` persiste-t-elle en mémoire une fois la boucle terminée ?
- 3/ Que fait la fonction `range` ? Pourquoi est-elle particulièrement utile dans le cadre des boucles `for` ?
- 5/ Comment fonctionne une boucle `while` ?
- 6/ Quand s'arrête une boucle `while` ? En quoi cela diffère-t-il des boucles `for` ?
- 7/ Pourquoi dit-on des listes et des tuples que ce sont des conteneurs ?
- 8/ Quel est le point commun entre les listes et les chaînes de caractères ?
- 9/ Comment est enregistré l'ordre des éléments dans une séquence en Python ?
- 12/ Peut-on avoir des éléments de types différents (ex : `int` et `string`) dans une même liste ? Dans un même tuple ?
- 13/ Peut-on accéder au iième élément d'un dictionnaire ?
- 14/ Quels types d'objets peuvent être utilisés comme clés d'un dictionnaire ? Comme valeurs ?
- 15/ Pour quels types de données a-t-on intérêt à utiliser un dictionnaire ?
- 16/ Un dictionnaire peut-il avoir des doublons dans ses clés ?

## Exercices

### Exercice 1

Afficher le type de x lorsque :

- x = 3
- x = "test"
- x = 3.5

### Exercice 2

Calculer la somme des longueurs de chacune des chaînes de caractères suivantes :

- "une première chaîne"
- "et une deuxième"
- "jamais deux sans trois"

### Exercice 3

Quel est le type adapté pour définir un code postal ?

Essayer de définir les codes postaux suivants au format `int` et au format `string` :

- 92120
- 02350

Que concluez-vous ?

### Exercice 4

Compter le nombre de fois où la lettre e est présente dans la chaîne suivante :
"Je fais un comptage des e."

**Indice** : on peut utiliser la méthode _built-in_ [find](https://docs.python.org/fr/3/library/stdtypes.html#str.find).

### Exercice 5

Repérer la première position où la lettre e est présente dans la chaîne suivante : "Je fais un comptage des e."

**Indice** : on peut utiliser la méthode _built-in_ [index](https://docs.python.org/fr/3/library/stdtypes.html#str.index).

### Exercice 6

Supprimer les espaces superflus au début et à la fin de la chaîne suivante :

```python
# Tapez votre réponse dans cette cellule
a = "    Un string très mal formatté.         "
```

### Exercice 7

Le caractère `\` permet d'échapper (neutraliser) un caractère spécial au sein d'une chaîne de caractères. Trouvez comment ce caractère permet de résoudre le problème lié à l'utilisation de guillemets (ou d'apostrophes) dans une chaîne définie par des guillemets (apostrophe).

**Indice** : des exemples d'utilisation sont disponibles dans la [documentation officielle](https://docs.python.org/fr/3.8/reference/lexical_analysis.html#literals).

### Exercice 8

Réaliser la suite d'opérations suivantes à l'aide des opérateurs d'incrémentation, et imprimer la valeur finale :

- initialiser une variable à 1
- lui soustraire 5
- la multiplier par 4
- lui ajouter 22

### Exercice 9

Considérons les deux séquences suivantes :

- "nous sommes en"
- "2022"

Trouvez à partir du tutoriel deux manières différentes de les utiliser pour composer la séquence "nous sommes en 2022".

**Indice** : l'une des deux méthodes implique de modifier (légèrement) une des deux séquences.

### Exercice 10

Constituez une liste semaine contenant les 7 jours de la semaine.

- À partir de cette liste, comment récupérez-vous seulement les 5 premiers jours de la semaine d'une part, et ceux du week-end d'autre part ? Utilisez pour cela l'indiçage.
- Cherchez un autre moyen pour arriver au même résultat (en utilisant un autre indiçage).
- Trouvez deux manières pour accéder au dernier jour de la semaine.

### Exercice 11

Soit la liste ["vache", "souris", "levure", "bacterie"].

- Affichez l'ensemble des éléments de cette liste (un élément par ligne) de trois manières différentes (deux avec for et une avec while).

### Exercice 12

Voici les notes d'un étudiant [14, 9, 6, 8, 12].

- Calculez la moyenne de ces notes.

### Exercice 13

Calculer le minimum et le maximum de la série de valeurs suivantes, sans utiliser les fonctions `min` et `max` de Python.

x = [8, 18, 6, 0, 15, 17.5, 9, 1]

### Exercice 14

Le but de cet exercice est d'implémenter le jeu du juste prix

on pourra utiliser la fonction **input()** et le code suivant pour obtenir un nombre alèatoire:

```python
import random as rd
nombre_mystere = rd.randrange(1000)
```

### Exercice 15: Suite de Fibonacci

<!-- #region tags=[] -->
La suite de Fibonacci se définit de la manière suivante : 
- les deux premiers nombres sont 0 et 1
- chaque autre nombre de la suite s'obtient en additionnant les deux nombres qui le précèdent

Ecrire un programme permettant de calculer les $n$ premiers termes de la suite à l'aide d'une boucle `for`.

### Exercice 16 : Sauvegarder votre travail

```bash
git checkout -b tp1-perso
git add .
git commit -m "fin tp1"
git push
```
