<!-- .slide: data-state="no-toc-progress" -->

# Initiation Git et Gitlab

<!-- .element: class="no-toc-progress" -->

[Donatien ENEMAN](https://gitlab.com/Donatien26) (donatien.eneman@insee.fr)

Online : [https://donatien26.gitlab.io/website/](https://donatien26.gitlab.io/website/)

Source : [https://gitlab.com/Donatien26/website](https://gitlab.com/Donatien26/website)

Ecrit en [markdown][1] et propulsé par [reveal.js](http://lab.hakim.se/reveal-js/)

[1]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
