# Quelques bonus

<!-- .slide: class="slide" -->

---

## Encrypter un mot de passe

<!-- .slide: class="slide" -->
Dans le dépot du control repo ou j'ai besoin d'un mdp :

```bash
echo mon_password | openssl smime -encrypt -stream -md sha256 -outform DER public_key.pkcs7.pem | base64 -w 0
```

Il n'y a plus qu'a rajouter ENC["PKCS7",LE_MDP_CRYPTE_DU_DESSUS]
---
