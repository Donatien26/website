# Git

<!-- .slide: class="slide" -->

---

## C'est quoi git ?

<!-- .slide: class="slide" -->

- C'est un outil de versionnage.

---

## 🤔 C'est quoi le versionnage ?

<!-- .slide: class="slide" -->

Une équipe projet classique se lance dans un nouveau projet. l'équipe est composé de :

- devs
- Maitrise d'ouvrage (votre client)
- un expert technique (soutien passager)
- Un chef de projet

---

### Un monde sans versionnage :

<!-- .slide: class="slide" -->

Un dossier projet partagé entre tout les devs 📁

- Quand une personne est en train de travailler sur un fichier, ce fichier se verrouille empêchant toutes autre personne de travailler. Tous les logiciels ne font pas ça, alors l'équipe passe sur un logiciel qui ne verrouille pas les documents ;
- Quand une personne modifie le code d'un fichier se fichier ne fonctionne plus tant que la modification n'est pas terminée et stable. Donc l'équipe va copier coller le dossier sur leur machine, faire les modification et copier/coller les modifications ;
- Cela crée un nouveau problème, par moment des modifications sont effacées lorsque des modification sont collées sur le serveur partagé et il n'y a aucun moyen de revenir en arrière, n'y d'avoir un historique des modifications.

Au bout d'une semaine l'équipe passe plus de temps à résoudre des bugs qu'a coder 🚨

---

### Versionnage à la main

<!-- .slide: class="slide" -->

- tout les soirs on créé un dossier à la main avec le code du jour

![Image no git](img/versionning-no-git.JPG)

Au bout d'une semaine l'équipe commence a s'entretuer... 🚨

Et on ne parle pas que l'équipe voudrait pouvoir développer des features chacun de leur coté avant de les fusionnée tous ensemble...

---

### Git la solution

<!-- .slide: class="slide" -->

Git est un logiciel de versionnage de code source. Il repose sur une architecture décentralisée. Il a été créé par Linus Torvalds, pour gérer les contributions au noyau Linux. Le fonctionnement de git est loin d'être trivial, et le coût d'entrée est loin d'être nul. Mais il est très largement utilisé en entreprise et ne pas savoir comment git fonctionne est aujourd'hui un défaut pour un data scientist.

---

### Ouais mais comment ca marche ?

<!-- .slide: class="slide" -->

<img src="img/gitDébutant.png" width="800"/>

---

### Différence avec svn

<!-- .slide: class="slide" -->

<img src="img/git-centralized-vs-distributed.png" width="800"/>

---

## Un peu de vocabulaire

- commit : photographie du code a un moment donné
- branche : historique
- tag: commit marqué comme particulier
- fork : copie d'un dépot ou vous n'avez pas les droits dans votre dossier et qui vit sa vie indépendamment du premier
- dépot: Un répertoire est un espace de stockage où vos projets peuvent vivre. Il peut être local sur un répertoire de votre ordinateur, ou ce peut être un espace de stockage sur GitHub, Gitlab ou tout autre hébergeur en ligne. À l’intérieur d’un dépôt, Vous pouvez conserver l'ensemble des fichiers de code, des fichiers texte, des images ainsi que leur histoire passée

---

## Création du dépot sur votre machine

<!-- .slide: class="slide" -->

```bash
git clone urlDeMonDepot.git
cd nomDeMonDepot
```

- Copie complète du dépot distant sur votre pc

---

## Lister les fichiers modifiés

<!-- .slide: class="slide" -->

- `git status`

---

## Ignorer des fichiers

<!-- .slide: class="slide" -->

Fichier `.gitignore`

- a la racine du dossier
- nom important
- Evite d'envoyer des fichiers qu'on ne veut pas partager

---

## Recupérer du code

<!-- .slide: class="slide" -->

- Dans le terminal (se placer à la racine du projet)

```bash
git pull
```

---

## Envoyer du code

<!-- .slide: class="slide" -->

- Dans le terminal (se placer à la racine du projet)

```bash
git add .
git commit -m "Un commentaire qui dit a quoi sert le code rajouté"
git push
```

⚠️ les deux premières commandes ne mettent a jour que votre copie du dépot locale. Bien penser a pusher pour partager avec les autres.

⚠️ Toujours pull avant de push afin de s'assurer que l'on a bien la dernière version du code

---

## Gérer les conflits 💣

<!-- .slide: class="slide" -->

Travailler sur le même fichier va (probablement) vous amener un gérer un conflit.

C'est quoi un conflit ?

2 versions du code vont modifier le meme fichier, git est intelligent mais il se peut qu'a un moment il ne soit pas capable de fusionner votre code.

---

### Comment je sais qu'il y a conflit ?

<!-- .slide: class="slide" -->

- Un conflit apparait lors d'un git pull
- Je le vois dans mon code :

```bash
<<<<<<< HEAD (Current Change)
Master branch
=======
Typo branch
>>>>>>> typo (Incoming CHange)
```

- Je résous mes conflits (demo)
- Puis :

```bash
git add fichier-conflit
git commit -m "Message de résolution du conflit."
```

- Si je veux tout annuler et revenir avant le git pull:

```bash
git merge --abort
```

---

## Lister les branches

<!-- .slide: class="slide" -->

- lister les branches locales:

```
git branch
```

- lister les branches distantes:

```
git branch -r
```

- lister toutes les branches:

```
git branch -a
```

---

## Changer de branche

<!-- .slide: class="slide" -->

- Si la branche existe déja (penser à bien avoir au moins commiter les changements de la branche courante):

```shell
git checkout leNomDeLaBranche
```

- Si la branche n'existe pas :
  (l'option -b permet de creer la branche dans votre dépot local)

```shell
git checkout -b leNomDeLaBranche
```

---

## Annuler tout les changements sur son depot local

<!-- .slide: class="slide" -->

- ⚠️ Action irréversible

```bash
- git pull
- git reset --hard origin
```

---

## Git merge/rebase

<!-- .slide: class="slide" -->

![Merge vs Rebase](img/merge_vs_rebase.drawio.png)
