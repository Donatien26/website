# Autres

---

## Pylint

- Scanner le code a la recherche du respect des bonnes pratiques de code
- Installation: `poetry add -D pylint ou pip install pylint`
- Utilisation: `find . -type f -name "*.py" | xargs pylint`

---

## Isort

- Ordonner les imports
- Installation: `poetry add -D isort ou pip install isort`
- Utilisation: `isort .`

---

## Black
- Formatter le code
- Installation: `poetry add -D black ou pip install black`
- Utilisation: `black .`