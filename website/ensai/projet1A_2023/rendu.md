# Le rendu

<!-- .slide: class="slide" -->

---

## Le code

<!-- .slide: class="slide" -->

- Un code fonctionnel !!! 
- Un code indenté et lisible
- Un code documenté
- Un code avec des tests
- Un code formatté
--> Tout votre code doit également être disponible sur git

---

## Le Rapport

<!-- .slide: class="slide" -->

- Un rapport de 15 pages (intro / mise en contexte + developpement + conclusion)
- Des diagrammes UML: Cas utilisation + Diagramme de classe correspondant a votre code. (a minima c'est 2 là)
- Des explications sur des choses qui vous simple importante (justification de votre choix d'implémentation)
- ce qui a marché ce qui a pas marché.
- Retour d'expérience qu'est ce qui faudrait améliorer ?

---

## La soutenance

<!-- .slide: class="slide" -->

- Pas de gros pavé de code sauf si y a un vrai interet
- Organisation de l'équipe
- présenter votre projet, comment il marche ?
- Démo
- question
