# Les Tests

---

## Pourquoi tester ?

- Verifier que l'application ne plante
- l'application réponds a mon besoin
- Vérifier qu'un ajout de code n'a pas cassé l'ancien code

---

## Les grands types de tests

- tests unitaires: test d'une fonction ou d'une partie du code
- test intégration: test du programme dans sa globalité
- test utilisateurs: vous qui testez votre appli

---

## Tester avec Pytest
- un dossier tests
- des fichiers test_{qqch} (un par module)
- des fonctions test_qqch() (une fonction a tester c'est potentiellement plusieurs tests !) 
- Pour lancer `pytest` : `pytest .`

---
## Demo

