# Gestion de packages/dependances

---

## C'est quoi un package

- Du code réutilisable
- Une bibliothèque de fonctions
- https://pypi.org/ -> store de packages

---

## Pip

- Le gestionnaire de paquets natif python
- `pip install le_paquet`
- Comment partager ça avec les autres ? -> requirements.txt

```txt
###### Requirements without Version Specifiers ######
nose
nose-cov
beautifulsoup4

###### Requirements with Version Specifiers ######
docopt == 0.6.1             # Version Matching. Must be version 0.6.1
keyring >= 4.1.1            # Minimum version 4.1.1
coverage != 3.5             # Version Exclusion. Anything except version 3.5
Mopidy-Dirble ~= 1.1        # Compatible release. Same as >= 1.1, == 1.*
```

---

## Poetry gerer ses dépendances en 2023

- Une nouvelle manière de gerer ses dépendances
- Facilite la création de package
- Le cycle de vie des packages de l'application
- Création du virtualenv automatique

---

### Installation

```shell
pip install poetry
poetry config virtualenvs.in-project true
```

---

### Mise en place

- Créer un package dans un terminal :

```shell
poetry new nom_du_package
```

- Installer un package dans un terminal :

```shell
poetry add ...
```

---

## Installer un projet cloné

```
poetry install .
```

---

### Démo
