# Préparation de votre poste

<!-- .slide: class="slide" -->

---

<!-- .slide: class="slide" -->

Toutes les choses à savoir pour configurer votre poste 😊

⚠️ Sur la VM de l'ENSAI aucune des opérations suivantes n'est nécéssaire.

---

## Installation python (Windows)

<!-- .slide: class="slide" -->

- https://www.python.org/downloads/ --> télécharger la dernière version
- Bien cliquer Add Python 3.x.y to PATH (Ajouter Python 3.... à PATH).
- Une fois installation terminée ouvrir un terminal (cmd ou powershell) et taper :

```bash
C:\Windows\System32> python --version
Python 3.x.y
```

---

## Installation python (MacOs)

<!-- .slide: class="slide" -->

- https://www.python.org/downloads/ --> télécharger la dernière version
- Bien cliquer Add Python 3.x.y to PATH (Ajouter Python 3.... à PATH).
- Une fois installation terminée ouvrir un terminal et taper :

```bash
C:\Windows\System32> python --version
Python 3.x.y
```

---

## Installation vscode (MacOS et Windows)

<!-- .slide: class="slide" -->

- ici : [https://code.visualstudio.com/download](https://code.visualstudio.com/download)

---

## Installer les extensions Vscode

<!-- .slide: class="slide" -->

- Vscode permet de développer en plein de language différent.
- A l'installation c'est une coquille vide, il est moins bon qu'un éditeur de texte

---

<!-- .slide: class="slide" -->

![Image no git](img/vs_code_extensions.PNG)

- Python Extension Pack
- Python Docstring Generator (Génération de la doc)


---

## Installation pycharm

<!-- .slide: class="slide" -->


- Ici : [https://www.jetbrains.com/fr-fr/community/education/#students](https://www.jetbrains.com/fr-fr/community/education/#students)
- Vous etes étudiant vous pouvez avoir une licence pycharm avec plein de chose en plus.
- Mettre votre adresse ENSAI

---

## Installation git

<!-- .slide: class="slide" -->

- installation de gitBash (Windows): [https://gitforwindows.org/](https://gitforwindows.org/)
- installation git-scm (MacOS): [https://sourceforge.net/projects/git-osx-installer/](https://sourceforge.net/projects/git-osx-installer/)
- Ouvrir terminal et taper git, un texte semblable devrait s'afficher :

```bash
test ~/random/path > git
usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone             Clone a repository into a new directory
   init              Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add               Add file contents to the index
   mv                Move or rename a file, a directory, or a symlink
   restore           Restore working tree files
   rm                Remove files from the working tree and from the index
   sparse-checkout   Initialize and modify the sparse-checkout
```

---

## Configurer git

<!-- .slide: class="slide" -->


- Pour pouvoir envoyer du code faire une fois

```
git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```
- Pensez à remplacer "John Doe" par votre Prénom nom et pareil pour l'email
