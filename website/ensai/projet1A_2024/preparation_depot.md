# Préparation dépot de code
<!-- .slide: class="slide" -->

---

## Git
<!-- .slide: class="slide" -->

- Git est obligatoire ! Pourquoi :
    * C'est utile en entreprise donc autant se forcer a l'utiliser dès maintenant
    * Ca me permettra de vous aider si nécéssaire
    * Ca evitera de tout perdre et de recommencer à 0

---

## Le dépot de code:
<!-- .slide: class="slide" -->

- Un dépot Gitlab ! et uniquement Gitlab !
- Pensez à m'ajouter en tant que membre de votre dépot !
- Pour initialiser vous pouvez vous inspirer de https://gitlab.com/Donatien26/template-1A-poo. Vous pouvez même copier tout ce qu'il y a dedans. Je souhaite retrouver une organisation du code semblable sur votre projet !
- Sur gitlab le fichier .gitlab-ci permet d'automatiser le lancement de certaines taches. Pensez à le rajouter dans votre dépot.


