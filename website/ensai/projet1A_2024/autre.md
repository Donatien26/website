# Autres
<!-- .slide: class="slide" -->

---

## InquirerPy
<!-- .slide: class="slide" -->

- Une dépendance pour faire des interfaces graphique
- Doc disponible ici https://inquirerpy.readthedocs.io/en/latest/index.html#

- Demo

---

## Pylint
<!-- .slide: class="slide" -->

- Scanner le code a la recherche du respect des bonnes pratiques de code
- Installation: `poetry add -D pylint ou pip install pylint`
- Utilisation: `find . -type f -name "*.py" | xargs pylint`

---

## Isort
<!-- .slide: class="slide" -->

- Ordonner les imports
- Installation: `poetry add -D isort ou pip install isort`
- Utilisation: `isort .`

---

## Black
<!-- .slide: class="slide" -->

- Formatter le code
- Installation: `poetry add -D black ou pip install black`
- Utilisation: `black .`