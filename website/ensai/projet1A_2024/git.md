# Git

<!-- .slide: class="slide" -->

---

## C'est quoi git ?

<!-- .slide: class="slide" -->

- C'est un outil de versionnage.

---

## 🤔 C'est quoi le versionnage ?

<!-- .slide: class="slide" -->

Une équipe projet classique se lance dans un nouveau projet. l'équipe est composé de :

- devs
- Maitrise d'ouvrage (votre client)
- un expert technique (soutien passager)
- Un chef de projet

---

### Un monde sans versionnage :

<!-- .slide: class="slide" -->

Un dossier projet partagé entre tout les devs 📁

- Quand une personne est en train de travailler sur un fichier, ce fichier se verrouille empêchant toutes autre personne de travailler. Tous les logiciels ne font pas ça, alors l'équipe passe sur un logiciel qui ne verrouille pas les documents ;
- Quand une personne modifie le code d'un fichier se fichier ne fonctionne plus tant que la modification n'est pas terminée et stable. Donc l'équipe va copier coller le dossier sur leur machine, faire les modification et copier/coller les modifications ;
- Cela crée un nouveau problème, par moment des modifications sont effacées lorsque des modification sont collées sur le serveur partagé et il n'y a aucun moyen de revenir en arrière, n'y d'avoir un historique des modifications.

Au bout d'une semaine l'équipe passe plus de temps à résoudre des bugs qu'a coder 🚨

---

### Versionnage à la main

<!-- .slide: class="slide" -->

- tout les soirs on créé un dossier à la main avec le code du jour

![Image no git](img/versionning-no-git.JPG)

Au bout d'une semaine l'équipe commence a s'entretuer... 🚨

---

### Git la solution

<!-- .slide: class="slide" -->

Git est un logiciel de versionnage de code source. Il repose sur une architecture décentralisée. Il a été créé par Linus Torvalds, pour gérer les contributions au noyau Linux. Le fonctionnement de git est loin d'être trivial, et le coût d'entrée est loin d'être nul. Mais il est très largement utilisé en entreprise et ne pas savoir comment git fonctionne est aujourd'hui un défaut pour un data scientist.

---

### Ouais mais comment ca marche ?

<!-- .slide: class="slide" -->

<img src="img/gitDébutant.png" width="800"/>

---

## Création d'un compte sur Gitlab

<!-- .slide: class="slide" -->

ici : [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up)

---

## Création de vos clés ssh (Sur votre poste)

<!-- .slide: class="slide" -->

- ⚠️ Toute cette manip ne fonctionnera que sur votre poste courant il faudra la refaire si vous changez de poste
- Ouvrir terminal (MacOs) ou GitBash (Windows)
- `ssh-keygen -t rsa -b 4096 -C "VotreAdresseEmail@example.com"`
- puis appuyez entrer jusqu'a la fin !!!
- `cat $HOME/.ssh/id_rsa.pub` --> on cherche ce fichier et on copie ce qu'il y a dedans

---

## Ajout de votre nouvelle clé ssh (Sur gitlab)

<!-- .slide: class="slide" -->

- Se connecter et cliquer sur votre avatar en haut a droite
- Cliquer Settings (ou preférences)
- Sur le coté cliquer SSH Keys (clefs SSH)
- Dans le grand carré coller ce que vous venez de copier
- Cliquer sur ajouter une clef

---

## Création du dépot (Un membre par groupe)

<!-- .slide: class="slide" -->

- Se rendre sur gitlab et creer un projet
- ajout de vos camarade en tant que maintener sur le projet

---

## Création du dépot sur votre machine

<!-- .slide: class="slide" -->

```bash
git clone urlDeMonDepot.git
cd nomDeMonDepot
```

---

## Ignorer des fichiers

<!-- .slide: class="slide" -->

Fichier `.gitignore`

- a la racine du dossier
- nom important
- Evite d'envoyer des fichiers qu'on ne veut pas partager

---

## Recupérer du code

<!-- .slide: class="slide" -->

- Dans le terminal (se placer à la racine du projet)

```bash
git pull
```

---

## Envoyer du code

<!-- .slide: class="slide" -->

- Dans le terminal (se placer à la racine du projet)

```bash
git add .
git commit -m "Un commentaire qui dit a quoi sert le code rajouté"
git push
```

⚠️ les deux premières commandes ne mettent a jour que votre copie du dépot locale. Bien penser a pusher pour partager avec les autres.

⚠️ Toujours pull avant de push afin de s'assurer que l'on a bien la dernière version du code

---

## Gérer les conflits 💣

<!-- .slide: class="slide" -->

Travailler sur le même fichier va (probablement) vous amener un gérer un conflit.

C'est quoi un conflit ?

2 versions du code vont modifier le meme fichier, git est intelligent mais il se peut qu'a un moment il ne soit pas capable de fusionner votre code.

---

### Comment je sais qu'il y a conflit ?

<!-- .slide: class="slide" -->

- Un conflit apparait lors d'un git pull
- Je le vois dans mon code :

```bash
<<<<<<< HEAD (Current Change)
Master branch
=======
Typo branch
>>>>>>> typo (Incoming CHange)
```

- Je résous mes conflits (demo)
- Puis :

```bash
git add fichier-conflit
git commit -m "Message de résolution du conflit."
```

- Si je veux tout annuler et revenir avant le git pull:

```bash
git merge --abort
```

---

### Avec l'interface VSCode
<!-- .slide: class="slide" -->

DEMO
