<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

# Projet POO 1A 2024

<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

[Donatien ENEMAN](https://gitlab.com/Donatien26) (donatien.eneman@insee.fr)

Online : [https://donatien26.gitlab.io/website/](https://donatien26.gitlab.io/website/)

<img src="./img/qrcode.png" alt="qrcode" width="100"/>

[1]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
