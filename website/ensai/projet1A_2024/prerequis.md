# Les prérequis

<!-- .slide: class="slide" -->

---

## Ce dont on a besoin :

<!-- .slide: class="slide" -->

<!-- .element: class="fragment" -->
- Python
<!-- .element: class="fragment" -->
- Un IDE (environnement de developpement) :
<!-- .element: class="fragment" -->
    * Vscode ou Pycharm (perso je prefere vscode) -> Je n'aiderai pas sur les autres.
<!-- .element: class="fragment" -->
- un client git
<!-- .element: class="fragment" -->
- Un compte Gitlab
<!-- .element: class="fragment" -->
- Un dépot gitlab
<!-- .element: class="fragment" -->
