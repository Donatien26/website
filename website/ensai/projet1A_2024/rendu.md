# Le rendu

<!-- .slide: class="slide" -->

---

## Le code

<!-- .slide: class="slide" -->

- Un code fonctionnel !!! 
- Un fichier requirements.txt avec la liste des dependances à installer. :warning: Respecter le formalisme du fichiers requirements.txt. 
- Un code indenté et lisible
- Un code documenté
- Un code avec des tests unitaires.
- Un code formatté
- un fichier main que je peux executer pour lancer et découvrir votre code.
--> Tout votre code doit également être disponible sur git, vous me fournirez l'url du dépot git dans le rapport final. Le code disponible sur le dépot devra être identique a celui que vous m'aurez rendu

---

## Comment je note le code:
<!-- .slide: class="slide" -->

- Je lance une analyse pylint:
    * Si la note est > 0 je divise par 10 et c'est du point bonus
    * Si la note est < 0 ce sont des points malus
- Un code que je n'arrive pas a lancer c'est forcément < 15
- Des points sont donnés pour la cohérence avec le diagramme de classe.
- Des points sont donnés pour la performance du code (si on attends pas trop longtemps pour un résultat).

---

## Le Rapport

<!-- .slide: class="slide" -->

- Un rapport au format latex.
- Un rapport de 15 pages (environ). Il comportera comme le rapport intermédiaire une partie intro,mise en contexte, caractéristique fonctionnelle, caractéristique technique, un retour d'expérience (comment vous avez vécu le projet, qu'est ce que vous changeriez,...) et une conclusion.
- Le rapport devra contenir des diagrammes UML: Cas utilisation + Diagramme de classe correspondant a votre code. (a minima c'est 2 là) Il peut il y en avoir d'autre !
- Des explications sur des choses qui vous simple importante (notamment justification de votre choix d'implémentation). Je ne veux pas juste une description des classes, mais plutot pourquoi cette classe/méthode.
- ce qui a marché ce qui a pas marché.
- Retour d'expérience qu'est ce qui faudrait améliorer ?

🚧 Le rapport doit être cohérent avec le code !

---

## La soutenance

<!-- .slide: class="slide" -->
- 40 minutes dont 10min prez / 10min demo / 10 min question.
- Vous organisez la premiere partie comme vous le souhaitez.

---

<!-- .slide: class="slide" -->

- Pas de gros pavé de code sauf si y a un vrai interet
- Organisation de l'équipe
- présenter votre projet, comment il marche ?
- Démo
- question

---

<!-- .slide: class="slide" -->

- 🚀 Le but de la soutenance est de présenter votre projet à un jury qui n'a que 20 min pour découvrir ce que vous vouliez faire et apprécier comment vous avez travailler ensemble, soyez clair et concis ! N'hésitez pas à mettre des shémas. 
- ⚠️ Par exemple, pour le diagramme de cas d'utilisation on pourra songer a une autre manière de le présenter que de mettre ce gros schéma 😊 